package com.acs.dao;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import com.acs.model.Livro;

public class LivroDao {

	public void salvar(Livro livro) {
		EntityManagerFactory factory =
		Persistence.createEntityManagerFactory("testAcs");
		EntityManager em = factory.createEntityManager();
		em.persist(livro);
	}
	
}
